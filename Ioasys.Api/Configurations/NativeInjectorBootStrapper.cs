﻿using Ioasys.Domain.Commands.Handlers;
using Ioasys.Domain.Interfaces;
using Ioasys.Infra.Context;
using Ioasys.Infra.Repositories;
using Ioasys.Infra.Transactions;
using Microsoft.Extensions.DependencyInjection;

namespace Ioasys.Api.Configurations
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IoasysDataContext, IoasysDataContext>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            //Repository
            services.AddTransient<IInvestorRepository, InvestorRepository>();
            services.AddTransient<IEnterpriseRepository, EnterpriseRepository>();

            //Commands Handlers
            services.AddTransient<InvestorCommandHandler, InvestorCommandHandler>();
            services.AddTransient<EnterpriseCommandHandler, EnterpriseCommandHandler>();
        }
    }
}
