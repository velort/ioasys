﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using FluentValidator;
using Ioasys.Api.Security;
using Ioasys.Domain.Commands.Inputs.InvestorCommands;
using Ioasys.Domain.Commands.Results;
using Ioasys.Domain.Entities;
using Ioasys.Domain.Interfaces;
using Ioasys.Domain.Models;
using Ioasys.Infra.Transactions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ioasys.Api.Controllers
{
    public class UsersController : BaseController
    {
        private Investor _investor;
        private readonly IInvestorRepository _repository;
        private readonly TokenOptions _tokenOptions;
        private readonly JsonSerializerSettings _serializerSettings;

        public UsersController(IOptions<TokenOptions> jwtOptions, IUnitOfWork uow, IInvestorRepository repository) : base(uow)
        {
            _repository = repository;
            _tokenOptions = jwtOptions.Value;
            ThrowIfInvalidOptions(_tokenOptions);

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("auth/sign_in")]
        public async Task<IActionResult> Post([FromBody] AuthenticateInvestorCommand command)
        {
            if (command == null)
                return await Response(null, new List<Notification> { new Notification("Investor", "Usuário ou senha inválidos") });

            var identity = await GetClaims(command);
            if (identity == null)
                return await Response(null, new List<Notification> { new Notification("Investor", "Usuário ou senha inválidos") });

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, command.Email),
                new Claim(JwtRegisteredClaimNames.NameId, command.Email),
                new Claim(JwtRegisteredClaimNames.Email, command.Email),
                new Claim(JwtRegisteredClaimNames.Sub, command.Email),
                new Claim(JwtRegisteredClaimNames.Jti, await _tokenOptions.JtiGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_tokenOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                identity.FindFirst("Ioasys")
            };

            var jwt = new JwtSecurityToken(
                issuer: _tokenOptions.Issuer,
                audience: _tokenOptions.Audience,
                claims: claims.AsEnumerable(),
                notBefore: _tokenOptions.NotBefore,
                expires: _tokenOptions.Expiration,
                signingCredentials: _tokenOptions.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);



            var response = new InvestorAccountCommandResult
            {
                Investor = new InvestorModel(_investor),
                Enterprise = null,
                Portifolio = new PortifolioModel(new List<Enterprise>()),
                Expires = (int) _tokenOptions.ValidFor.TotalSeconds,
                Token = encodedJwt,
                Success = true
            };


            return new OkObjectResult(response);
        }

        private static void ThrowIfInvalidOptions(TokenOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (options.ValidFor <= TimeSpan.Zero)
                throw new ArgumentException("O período deve ser maior que zero", nameof(TokenOptions.ValidFor));

            if (options.SigningCredentials == null)
                throw new ArgumentNullException(nameof(TokenOptions.SigningCredentials));

            if (options.JtiGenerator == null)
                throw new ArgumentNullException(nameof(TokenOptions.JtiGenerator));
        }

        private static long ToUnixEpochDate(DateTime date)
            => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);

        private Task<ClaimsIdentity> GetClaims(AuthenticateInvestorCommand command)
        {
            var user = _repository.GetByEmail(command.Email);

            if (user == null)
                return Task.FromResult<ClaimsIdentity>(null);

            if (!user.Authenticate(command.Email, command.Password))
                return Task.FromResult<ClaimsIdentity>(null);

            _investor = user;

            return Task.FromResult(new ClaimsIdentity(
                new GenericIdentity(user.Name, "Token"),
                new[] {
                    new Claim("Ioasys", "Investor")
                }));
        }
    }
}