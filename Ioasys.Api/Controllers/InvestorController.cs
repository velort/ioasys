﻿using System.Threading.Tasks;
using Ioasys.Domain.Commands.Handlers;
using Ioasys.Domain.Commands.Inputs.InvestorCommands;
using Ioasys.Infra.Transactions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ioasys.Api.Controllers
{
    public class InvestorController : BaseController
    {
        private readonly InvestorCommandHandler _handler;

        public InvestorController(IUnitOfWork unitOfWork, InvestorCommandHandler handler) : base(unitOfWork)
        {
            _handler = handler;
        }

        [HttpPost]
        [Route("users")]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] RegisterInvestorCommand command)
        {
            var result = _handler.Handle(command);
            return await Response(result, _handler.Notifications);
        }
    }
}
