﻿using System.Threading.Tasks;
using Ioasys.Domain.Commands.Handlers;
using Ioasys.Domain.Commands.Inputs.EnterpriseCommands;
using Ioasys.Domain.Commands.Inputs.InvestorCommands;
using Ioasys.Domain.Interfaces;
using Ioasys.Infra.Transactions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ioasys.Api.Controllers
{
    public class EnterprisesController : BaseController
    {
        private readonly EnterpriseCommandHandler _handler;
        private readonly IEnterpriseRepository _repository;

        public EnterprisesController(IUnitOfWork unitOfWork, EnterpriseCommandHandler handler, IEnterpriseRepository repository) : base(unitOfWork)
        {
            _handler = handler;
            _repository = repository;
        }

        [HttpPost]
        [Route("enterprise")]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] RegisterEnterpriseCommand command)
        {
            var result = _handler.Handle(command);
            return await Response(result, _handler.Notifications);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            var saida = _repository.GetById(id);
            return Ok(saida);
        }

        [HttpGet]
        [Route("enterprises{enterprise_types},{name}")]
        public IActionResult GetByFilter(int enterprise_types, string name)
        {
            var saida = _repository.GetFilter(enterprise_types, name);
            return Ok(saida);
        }

        [HttpGet]
        [Route("enterprises")]
        public IActionResult GeAll()
        {
            var saida = _repository.GetAll();
            return Ok(saida);
        }
    }
}