﻿using FluentValidator;

namespace Ioasys.Domain.Entities
{
    public class Entity : Notifiable
    {
        public int Id { get; set; }
    }
}
