﻿using System.Text;
using FluentValidator.Validation;

namespace Ioasys.Domain.Entities
{
    public class Investor : Entity
    {
        protected Investor() { }

        public Investor(string name, string email, string city, string country, int balance, string photo, string password, int portifolioValue, bool firstAcces, bool superAngel)
        {
            Name = name;
            Email = email;
            City = city;
            Country = country;
            Balance = balance;
            Photo = photo;
            Password = EncryptPassword(password);
            PortifolioValue = portifolioValue;
            FirstAcces = firstAcces;
            SuperAngel = superAngel;

            new ValidationContract().IsEmailOrEmpty(Email, "E-mail" , "E-mail inválido");
        }

        public string Name { get; private set; }
        public string Email { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }
        public int Balance { get; private set; }
        public string Photo { get; private set; }
        public string Password { get; private set; }
        public int PortifolioValue { get; private set; }
        public bool FirstAcces { get; private set; }
        public bool SuperAngel { get; private set; }


        public bool Authenticate(string username, string password)
        {
            if (Email == username && Password == EncryptPassword(password))
                return true;

            AddNotification("Investor", "Usuário ou senha inválidos");
            return false;
        }


        private string EncryptPassword(string pass)
        {
            if (string.IsNullOrEmpty(pass)) return "";
            var password = (pass += "|2d331cca-f6c0-40c0-bb43-6e32989c2881");
            var md5 = System.Security.Cryptography.MD5.Create();
            var data = md5.ComputeHash(Encoding.Default.GetBytes(password));
            var sbString = new StringBuilder();
            foreach (var t in data)
                sbString.Append(t.ToString("x2"));

            return sbString.ToString();
        }
    }
}
