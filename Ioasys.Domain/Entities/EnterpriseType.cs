﻿namespace Ioasys.Domain.Entities
{
    public class EnterpriseType : Entity
    {
        public EnterpriseType(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}
