﻿using FluentValidator.Validation;
using Ioasys.Domain.Commands.Inputs.EnterpriseCommands;

namespace Ioasys.Domain.Entities
{
    public class Enterprise : Entity
    {
        public Enterprise(string name, string description, string email, string facebook, string twitter, string linkedin, string phone, bool ownEnterprise, string photo, int value, int shares, double sharePrice, int ownShares, string city, string country, int enterpriseTypeId)
        {
            Name = name;
            Description = description;
            Email = email;
            Facebook = facebook;
            Twitter = twitter;
            Linkedin = linkedin;
            Phone = phone;
            OwnEnterprise = ownEnterprise;
            Photo = photo;
            Value = value;
            Shares = shares;
            SharePrice = sharePrice;
            OwnShares = ownShares;
            City = city;
            Country = country;
            EnterpriseTypeId = enterpriseTypeId;

            new ValidationContract().IsEmailOrEmpty(Email, "E-mail", "E-mail inválido");
        }

        public Enterprise(RegisterEnterpriseCommand enterpriseCommand)
        {
            Name = enterpriseCommand.Name;
            Description = enterpriseCommand.Description;
            Email = enterpriseCommand.Email;
            Facebook = enterpriseCommand.Facebook;
            Twitter = enterpriseCommand.Facebook;
            Linkedin = enterpriseCommand.Linkedin;
            Phone = enterpriseCommand.Phone;
            OwnEnterprise = enterpriseCommand.OwnEnterprise;
            Photo = enterpriseCommand.Photo;
            Value = enterpriseCommand.Value;
            Shares = enterpriseCommand.Shares;
            SharePrice = enterpriseCommand.SharePrice;
            OwnShares = enterpriseCommand.OwnShares;
            City = enterpriseCommand.City;
            Country = enterpriseCommand.Country;
            EnterpriseTypeId = enterpriseCommand.EnterpriseTypeId;
        }

        public string Name { get; private set; }
        public string Description { get; private set; }
        public string Email { get; private set; }
        public string Facebook { get; private set; }
        public string Twitter { get; private set; }
        public string Linkedin { get; private set; }
        public string Phone { get; private set; }
        public bool OwnEnterprise { get; private set; }
        public string Photo { get; private set; }
        public int Value { get; private set; }
        public int Shares { get; private set; }
        public double SharePrice { get; private set; }
        public int OwnShares { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }
        public int EnterpriseTypeId { get; private set; }

        public virtual EnterpriseType EnterpriseType { get; private set; }
    }
}
