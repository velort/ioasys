﻿
using Ioasys.Domain.Entities;

namespace Ioasys.Domain.Models
{
    public class InvestorModel
    {

        public InvestorModel(Investor investor)
        {
            Name = investor.Name;
            Email = investor.Email;
            City = investor.City;
            Country = investor.Country;
            Balance = investor.Balance;
            Photo = investor.Photo;
            PortifolioValue = investor.PortifolioValue;
            FirstAcces = investor.FirstAcces;
            SuperAngel = investor.SuperAngel;
        }

        public string Name { get;  set; }
        public string Email { get;  set; }
        public string City { get;  set; }
        public string Country { get;  set; }
        public int Balance { get;  set; }
        public string Photo { get;  set; }
        public int PortifolioValue { get;  set; }
        public bool FirstAcces { get;  set; }
        public bool SuperAngel { get;  set; }
    }
}
