﻿using System.Collections.Generic;
using Ioasys.Domain.Entities;

namespace Ioasys.Domain.Models
{
    public class PortifolioModel
    {
        public PortifolioModel(List<Enterprise> enterprises)
        {
            EnterprisesNumber = enterprises.Count;
            Enterprises = enterprises;
        }

        public int EnterprisesNumber { get; set; }
        public List<Enterprise> Enterprises { get; set; }
    }
}
