﻿using Ioasys.Domain.Entities;
using Ioasys.Shared.Singletons;
using System;
using System.Linq.Expressions;

namespace Ioasys.Shared.Filters
{
    public class EnterpriseFilter : Singleton<EnterpriseFilter>
    {
        public static Expression<Func<Enterprise, bool>> ByEnterpriseType(int id)
        {
            if ( id == 0)
            {
                return dec => true;
            }

            return dec => dec.EnterpriseType.Id.Equals(id);
        }

        public static Expression<Func<Enterprise, bool>> ByEnterpriseName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return dec => true;
            }

            return dec => dec.Name.ToLower().Contains(name.ToLower());
        }
    }
}
