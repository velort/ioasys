﻿using Ioasys.Domain.Entities;

namespace Ioasys.Domain.Interfaces
{
    public interface IInvestorRepository : IRepository<Investor>
    {
        Investor GetByEmail(string email);

        void Save(Investor investor);

        bool CheckEmail(string email);
    }
}
