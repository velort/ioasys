﻿using System.Collections.Generic;
using Ioasys.Domain.Entities;

namespace Ioasys.Domain.Interfaces
{
    public interface IEnterpriseRepository : IRepository<Enterprise>
    {
        Enterprise GetById(int id);

        void Save(Enterprise enterprise);

        List<Enterprise> GetAll();

        List<Enterprise> GetFilter(int idEnterpriseType, string nameEnterprise);

        bool CheckEmail(string email);
    }
}
