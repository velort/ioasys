﻿using Ioasys.Shared.Commands;

namespace Ioasys.Domain.Commands.Results
{
    public class EnterpriseCommandResult : ICommandResult
    {
        public EnterpriseCommandResult(object data)
        {
            Data = data;
        }

        public object Data { get; set; }
    }
}
