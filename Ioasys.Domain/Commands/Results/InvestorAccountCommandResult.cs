﻿using Ioasys.Domain.Entities;
using Ioasys.Domain.Models;
using Ioasys.Shared.Commands;

namespace Ioasys.Domain.Commands.Results
{
    public class InvestorAccountCommandResult : ICommand
    {
        public InvestorModel Investor { get; set; }
        public PortifolioModel Portifolio { get; set; }
        public Enterprise Enterprise { get; set; }
        public string Token { get; set; }
        public int Expires { get; set; }
        public bool Success { get; set; }
    }
}
