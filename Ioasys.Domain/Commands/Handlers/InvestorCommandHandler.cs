﻿using FluentValidator;
using Ioasys.Domain.Commands.Inputs.InvestorCommands;
using Ioasys.Domain.Commands.Outputs;
using Ioasys.Domain.Entities;
using Ioasys.Domain.Interfaces;
using Ioasys.Shared.Commands;

namespace Ioasys.Domain.Commands.Handlers
{
    public class InvestorCommandHandler : Notifiable, ICommandHandler<RegisterInvestorCommand>
    {
        private readonly IInvestorRepository _investorRepository;

        public InvestorCommandHandler(IInvestorRepository investorRepository)
        {
            _investorRepository = investorRepository;
        }

        public ICommandResult Handle(RegisterInvestorCommand command)
        {
            // Verificar se o E-mail já existe na base
            if (_investorRepository.CheckEmail(command.Email))
                AddNotification("Email", "Este E-mail já está em uso");

            // Criar os VOs
            var user = new Investor(command.Name, command.Email, command.City, command.Country, command.Balance, command.Photo, command.Password, command.PortifolioValue, true, command.SuperAngel);

            // Validar entidades e VOs
            AddNotifications(user.Notifications);

            if (Invalid)
                return new CommandResult(false, "Por favor, corrija os campos abaixo", Notifications);

            // Persistir o user
            _investorRepository.Save(user);


            // Retornar o resultado para tela
            return new CommandResult(true, "Bem vindo!", new
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email
            });
        }
    }
}
