﻿using FluentValidator;
using Ioasys.Domain.Commands.Inputs.EnterpriseCommands;
using Ioasys.Domain.Commands.Outputs;
using Ioasys.Domain.Commands.Results;
using Ioasys.Domain.Entities;
using Ioasys.Domain.Interfaces;
using Ioasys.Shared.Commands;

namespace Ioasys.Domain.Commands.Handlers
{
    public class EnterpriseCommandHandler : Notifiable, ICommandHandler<RegisterEnterpriseCommand>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        public EnterpriseCommandHandler(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }
        public ICommandResult Handle(RegisterEnterpriseCommand command)
        {
            // Verificar se o E-mail já existe na base
            if (_enterpriseRepository.CheckEmail(command.Email))
                AddNotification("Email", "Este E-mail já está em uso");

            // Criar os VOs
            var enterprise = new Enterprise(command);

            // Validar entidades e VOs
            AddNotifications(enterprise.Notifications);

            if (Invalid)
                return new CommandResult(false, "Por favor, corrija os campos abaixo", Notifications);

            // Persistir o user
            _enterpriseRepository.Save(enterprise);


            // Retornar o resultado para tela
            return new EnterpriseCommandResult(enterprise);
        }
    }
}
