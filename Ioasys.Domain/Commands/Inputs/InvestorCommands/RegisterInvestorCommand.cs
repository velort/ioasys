﻿using Ioasys.Shared.Commands;


namespace Ioasys.Domain.Commands.Inputs.InvestorCommands
{
    public class RegisterInvestorCommand : ICommand
    {
        public string Name { get;  set; }
        public string Email { get;  set; }
        public string City { get;  set; }
        public string Country { get;  set; }
        public int Balance { get;  set; }
        public string Photo { get;  set; }
        public string Password { get;  set; }
        public int PortifolioValue { get;  set; }
        public bool SuperAngel { get;  set; }
    }
}
