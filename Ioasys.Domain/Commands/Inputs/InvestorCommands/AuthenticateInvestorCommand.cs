﻿using Ioasys.Shared.Commands;

namespace Ioasys.Domain.Commands.Inputs.InvestorCommands
{
    public class AuthenticateInvestorCommand : ICommand
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}