﻿using Ioasys.Shared.Commands;

namespace Ioasys.Domain.Commands.Inputs.EnterpriseCommands
{
    public class RegisterEnterpriseCommand : ICommand
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        public bool OwnEnterprise { get; set; }
        public string Photo { get; set; }
        public int Value { get; set; }
        public int Shares { get; set; }
        public double SharePrice { get; set; }
        public int OwnShares { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int EnterpriseTypeId { get; set; }
    }
}
