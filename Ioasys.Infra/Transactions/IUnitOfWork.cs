﻿using System;

namespace Ioasys.Infra.Transactions
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}
