﻿using Ioasys.Infra.Context;

namespace Ioasys.Infra.Transactions
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IoasysDataContext _context;

        public UnitOfWork(IoasysDataContext context)
        {
            _context = context;
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
