﻿using Ioasys.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Infra.Mappings
{
    public class EnterpriseMap : IEntityTypeConfiguration<Enterprise>
    {
       public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.ToTable("enterprise");
            builder.Ignore(x => x.Notifications);

            builder.Property(x => x.Id)
                .HasColumnName("id");

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("enterprise_type_name");

            builder.Property(x => x.Description)
                .IsRequired()
                .HasColumnName("description");

            builder.Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("email_enterprise");

            builder.Property(x => x.Facebook)
                .HasMaxLength(200)
                .HasColumnName("facebook");

            builder.Property(x => x.Twitter)
                .HasMaxLength(200)
                .HasColumnName("twitter");

            builder.Property(x => x.Linkedin)
                .HasMaxLength(200)
                .HasColumnName("linkedin");

            builder.Property(x => x.Phone)
                .HasMaxLength(200)
                .HasColumnName("phone");

            builder.Property(x => x.OwnEnterprise)
                .IsRequired()
                .HasColumnName("own_enterprise");

            builder.Property(x => x.Photo)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("photo");

            builder.Property(x => x.Value)
                .IsRequired()
                .HasColumnName("value");

            builder.Property(x => x.Shares)
                .IsRequired()
                .HasColumnName("shares");

            builder.Property(x => x.SharePrice)
                .IsRequired()
                .HasColumnName("share_price");

            builder.Property(x => x.OwnShares)
                .IsRequired()
                .HasColumnName("own_shares");

            builder.Property(x => x.City)
                .IsRequired()
                .HasMaxLength(80)
                .HasColumnName("city");

            builder.Property(x => x.Country)
                .IsRequired()
                .HasMaxLength(80)
                .HasColumnName("country");

            builder.Property(x => x.EnterpriseTypeId)
                .IsRequired()
                .HasColumnName("id_enterprise_type");

            builder.HasOne(x => x.EnterpriseType);

        }
    }
}
