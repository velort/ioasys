﻿using Ioasys.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Infra.Mappings
{
    public class InvestorMap : IEntityTypeConfiguration<Investor>
    {
        public void Configure(EntityTypeBuilder<Investor> builder)
        {

            builder.ToTable("investor");
            builder.Ignore(x => x.Notifications);

            builder.Property(x => x.Id)
                .HasColumnName("id");

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("name");

            builder.Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("email");

            builder.Property(x => x.City)
                .IsRequired()
                .HasMaxLength(80)
                .HasColumnName("city");

            builder.Property(x => x.Country)
                .IsRequired()
                .HasMaxLength(80)
                .HasColumnName("country");

            builder.Property(x => x.Balance)
                .HasColumnName("balance");

            builder.Property(x => x.Photo)
                .HasColumnName("photo");

            builder.Property(x => x.Password)
                .IsRequired()
                .HasMaxLength(32)
                .HasColumnName("password");

            builder.Property(x => x.PortifolioValue)
                .IsRequired()
                .HasColumnName("portfolio_value");

            builder.Property(x => x.FirstAcces)
                .IsRequired()
                .HasColumnName("first_access");

            builder.Property(x => x.SuperAngel)
                .IsRequired()
                .HasColumnName("super_angel");
        }
    }
}
