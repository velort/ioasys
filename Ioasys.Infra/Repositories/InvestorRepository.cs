﻿using System.Linq;
using Ioasys.Domain.Entities;
using Ioasys.Domain.Interfaces;
using Ioasys.Infra.Context;
using Microsoft.EntityFrameworkCore;

namespace Ioasys.Infra.Repositories
{
    public class InvestorRepository : Repository<Investor>, IInvestorRepository
    {
        private readonly IoasysDataContext _context;

        public InvestorRepository(IoasysDataContext context) : base(context)
        {
            _context = context;
        }

        public Investor GetByEmail(string email)
        {
            return _context.Investor.AsNoTracking().FirstOrDefault(x => x.Email == email);
        }

        public bool CheckEmail(string email)
        {
            return _context.Investor.Any(x => x.Email == email);
        }

        public void Save(Investor investor)
        {
            _context.Investor.Add(investor);
        }
    }
}
