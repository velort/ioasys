﻿using System.Collections.Generic;
using System.Linq;
using Ioasys.Domain.Entities;
using Ioasys.Domain.Interfaces;
using Ioasys.Infra.Context;
using Ioasys.Shared.Filters;
using Microsoft.EntityFrameworkCore;
namespace Ioasys.Infra.Repositories
{
    public class EnterpriseRepository : Repository<Enterprise>, IEnterpriseRepository
    {
        private readonly IoasysDataContext _context;

        public EnterpriseRepository(IoasysDataContext context) : base(context)
        {
            _context = context;
        }

        public void Save(Enterprise enterprise)
        {
            _context.Enterprise.Add(enterprise);
        }

        public List<Enterprise> GetAll()
        {
            var saida = _context.Set<Enterprise>().Include(x => x.EnterpriseType).ToList();
            return saida;
        }

        public List<Enterprise> GetFilter(int idEnterpriseType, string nameEnterprise)
        {
            return _context.Set<Enterprise>()
                           .Include(x => x.EnterpriseType)
                           .Where(EnterpriseFilter.ByEnterpriseType(idEnterpriseType))
                           .Where(EnterpriseFilter.ByEnterpriseName(nameEnterprise))
                           .ToList();
        }

        public bool CheckEmail(string email)
        {
            return _context.Enterprise.Any(x => x.Email == email);
        }
    }
}
