﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ioasys.Infra.Migrations
{
    public partial class CorrecaoEnterprise : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_enterprise_enterprise_type_EnterpriseTypeId",
                table: "enterprise");

            migrationBuilder.DropIndex(
                name: "IX_enterprise_EnterpriseTypeId",
                table: "enterprise");

            migrationBuilder.DropColumn(
                name: "EnterpriseTypeId",
                table: "enterprise");

            migrationBuilder.CreateIndex(
                name: "IX_enterprise_id_enterprise_type",
                table: "enterprise",
                column: "id_enterprise_type");

            migrationBuilder.AddForeignKey(
                name: "FK_enterprise_enterprise_type_id_enterprise_type",
                table: "enterprise",
                column: "id_enterprise_type",
                principalTable: "enterprise_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_enterprise_enterprise_type_id_enterprise_type",
                table: "enterprise");

            migrationBuilder.DropIndex(
                name: "IX_enterprise_id_enterprise_type",
                table: "enterprise");

            migrationBuilder.AddColumn<int>(
                name: "EnterpriseTypeId",
                table: "enterprise",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_enterprise_EnterpriseTypeId",
                table: "enterprise",
                column: "EnterpriseTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_enterprise_enterprise_type_EnterpriseTypeId",
                table: "enterprise",
                column: "EnterpriseTypeId",
                principalTable: "enterprise_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
