﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ioasys.Infra.Migrations
{
    public partial class Investor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "investor",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    email = table.Column<string>(maxLength: 200, nullable: false),
                    city = table.Column<string>(maxLength: 80, nullable: false),
                    country = table.Column<string>(maxLength: 80, nullable: false),
                    balance = table.Column<int>(nullable: false),
                    photo = table.Column<string>(nullable: true),
                    password = table.Column<string>(maxLength: 32, nullable: false),
                    portfolio_value = table.Column<int>(nullable: false),
                    first_access = table.Column<bool>(nullable: false),
                    super_angel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_investor", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "investor");
        }
    }
}
