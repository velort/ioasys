﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ioasys.Infra.Migrations
{
    public partial class Investor_Name : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "investor",
                newName: "name");

            migrationBuilder.AlterColumn<string>(
                name: "name",
                table: "investor",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "name",
                table: "investor",
                newName: "Name");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "investor",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);
        }
    }
}
