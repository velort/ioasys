﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ioasys.Infra.Migrations
{
    public partial class Enterprise : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enterprise_type",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    enterprise_type_name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enterprise_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "enterprise",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    enterprise_type_name = table.Column<string>(maxLength: 200, nullable: false),
                    description = table.Column<string>(nullable: false),
                    email_enterprise = table.Column<string>(maxLength: 200, nullable: false),
                    facebook = table.Column<string>(maxLength: 200, nullable: true),
                    twitter = table.Column<string>(maxLength: 200, nullable: true),
                    linkedin = table.Column<string>(maxLength: 200, nullable: true),
                    phone = table.Column<string>(maxLength: 200, nullable: true),
                    own_enterprise = table.Column<bool>(nullable: false),
                    photo = table.Column<string>(maxLength: 200, nullable: false),
                    value = table.Column<int>(nullable: false),
                    shares = table.Column<int>(nullable: false),
                    share_price = table.Column<double>(nullable: false),
                    own_shares = table.Column<int>(nullable: false),
                    city = table.Column<string>(maxLength: 80, nullable: false),
                    country = table.Column<string>(maxLength: 80, nullable: false),
                    id_enterprise_type = table.Column<int>(nullable: false),
                    EnterpriseTypeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enterprise", x => x.id);
                    table.ForeignKey(
                        name: "FK_enterprise_enterprise_type_EnterpriseTypeId",
                        column: x => x.EnterpriseTypeId,
                        principalTable: "enterprise_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_enterprise_EnterpriseTypeId",
                table: "enterprise",
                column: "EnterpriseTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enterprise");

            migrationBuilder.DropTable(
                name: "enterprise_type");
        }
    }
}
