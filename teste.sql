USE [master]
GO
/****** Object:  Database [ioasys]    Script Date: 27/07/2019 17:23:48 ******/
CREATE DATABASE [ioasys]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ioasys', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\ioasys.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ioasys_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\ioasys_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ioasys] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ioasys].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ioasys] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ioasys] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ioasys] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ioasys] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ioasys] SET ARITHABORT OFF 
GO
ALTER DATABASE [ioasys] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ioasys] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ioasys] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ioasys] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ioasys] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ioasys] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ioasys] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ioasys] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ioasys] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ioasys] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ioasys] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ioasys] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ioasys] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ioasys] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ioasys] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ioasys] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ioasys] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ioasys] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ioasys] SET  MULTI_USER 
GO
ALTER DATABASE [ioasys] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ioasys] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ioasys] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ioasys] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ioasys] SET DELAYED_DURABILITY = DISABLED 
GO
USE [ioasys]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 27/07/2019 17:23:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[enterprise]    Script Date: 27/07/2019 17:23:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[enterprise](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[enterprise_type_name] [nvarchar](200) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[email_enterprise] [nvarchar](200) NOT NULL,
	[facebook] [nvarchar](200) NULL,
	[twitter] [nvarchar](200) NULL,
	[linkedin] [nvarchar](200) NULL,
	[phone] [nvarchar](200) NULL,
	[own_enterprise] [bit] NOT NULL,
	[photo] [nvarchar](200) NOT NULL,
	[value] [int] NOT NULL,
	[shares] [int] NOT NULL,
	[share_price] [float] NOT NULL,
	[own_shares] [int] NOT NULL,
	[city] [nvarchar](80) NOT NULL,
	[country] [nvarchar](80) NOT NULL,
	[id_enterprise_type] [int] NOT NULL,
 CONSTRAINT [PK_enterprise] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[enterprise_type]    Script Date: 27/07/2019 17:23:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[enterprise_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[enterprise_type_name] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_enterprise_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[investor]    Script Date: 27/07/2019 17:23:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[investor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NOT NULL,
	[email] [nvarchar](200) NOT NULL,
	[city] [nvarchar](80) NOT NULL,
	[country] [nvarchar](80) NOT NULL,
	[balance] [int] NOT NULL,
	[photo] [nvarchar](max) NULL,
	[password] [nvarchar](32) NOT NULL,
	[portfolio_value] [int] NOT NULL,
	[first_access] [bit] NOT NULL,
	[super_angel] [bit] NOT NULL,
 CONSTRAINT [PK_investor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190726210342_Investor', N'2.1.4-rtm-31024')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190726222657_Investor_Name', N'2.1.4-rtm-31024')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190726233757_Enterprise', N'2.1.4-rtm-31024')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190727200305_CorrecaoEnterprise', N'2.1.4-rtm-31024')
SET IDENTITY_INSERT [dbo].[enterprise] ON 

INSERT [dbo].[enterprise] ([id], [enterprise_type_name], [description], [email_enterprise], [facebook], [twitter], [linkedin], [phone], [own_enterprise], [photo], [value], [shares], [share_price], [own_shares], [city], [country], [id_enterprise_type]) VALUES (1, N'AQM S.A.', N'Cold Killer was discovered by chance in the ´90 s and developed by Mrs. Inés Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ', N'teste@teste', N'', N'', N'', N'', 0, N'/uploads/enterprise/photo/2/WhatsApp_Image_2017-10-31_at_13.47.22.jpeg', 0, 100, 5000, 0, N'Maule', N'Chile', 1)
SET IDENTITY_INSERT [dbo].[enterprise] OFF
SET IDENTITY_INSERT [dbo].[enterprise_type] ON 

INSERT [dbo].[enterprise_type] ([id], [enterprise_type_name]) VALUES (1, N'Teste')
SET IDENTITY_INSERT [dbo].[enterprise_type] OFF
SET IDENTITY_INSERT [dbo].[investor] ON 

INSERT [dbo].[investor] ([id], [name], [email], [city], [country], [balance], [photo], [password], [portfolio_value], [first_access], [super_angel]) VALUES (1, N'Teste Apple', N'testeapple@ioasys.com.br', N'BH', N'Brasil', 1000000, N'teste', N'27f00611e9fc74352985d390276884df', 1000000, 1, 0)
SET IDENTITY_INSERT [dbo].[investor] OFF
/****** Object:  Index [IX_enterprise_id_enterprise_type]    Script Date: 27/07/2019 17:23:48 ******/
CREATE NONCLUSTERED INDEX [IX_enterprise_id_enterprise_type] ON [dbo].[enterprise]
(
	[id_enterprise_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[enterprise]  WITH CHECK ADD  CONSTRAINT [FK_enterprise_enterprise_type_id_enterprise_type] FOREIGN KEY([id_enterprise_type])
REFERENCES [dbo].[enterprise_type] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[enterprise] CHECK CONSTRAINT [FK_enterprise_enterprise_type_id_enterprise_type]
GO
USE [master]
GO
ALTER DATABASE [ioasys] SET  READ_WRITE 
GO
